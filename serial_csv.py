#!/usr/bin/env python
import serial
import time

csv = open("data.csv", "a")
ser = serial.Serial('/dev/ttyACM0', 9600)

while True:
    line = ser.readline().decode('unicode_escape')
    csv.write(line)
    time.sleep(1)
    print(line)

ser.close()
