#include <Adafruit_MotorShield.h>

Adafruit_MotorShield afms;

auto left_motor = afms.getMotor(1);
auto right_motor = afms.getMotor(2);

// Declare ports
const uint8_t left_ir = A0;
const uint8_t right_ir = A1;

// The maximum speed for the robot.
uint8_t max_speed = 28;

// Used to print data once every 100 loops
uint8_t counter = 0;

void setup()
{
  Serial.begin(9600);

  // Setup Sensors
  pinMode(left_ir, INPUT);
  pinMode(right_ir, INPUT);
  afms.begin();

  // Setup Motors
  left_motor->setSpeed(128);
  right_motor->setSpeed(128);

  left_motor->run(BACKWARD);
  right_motor->run(BACKWARD);

  left_motor->run(RELEASE);
  right_motor->run(RELEASE);
}

void loop()
{
  // Get sensor values
  auto left = analogRead(left_ir);
  auto right = analogRead(right_ir);

  // Update speed if new speed is written to serial
  const uint8_t new_speed = Serial.parseInt();
  max_speed = new_speed ? new_speed : max_speed;

  // Map speeds to a range between off-line and on-line ranges.
  // These magic numbers are the off and on-line speeds.
  auto left_speed = map(left, 947, 882, 0, max_speed);
  auto right_speed = map(right, 960, 909, 0, max_speed);
  left_speed = left_speed > max_speed ? max_speed : left_speed;
  right_speed = right_speed > max_speed ? max_speed : right_speed;
  left_speed = left_speed < 0 ? 0 : left_speed;
  right_speed = right_speed < 0 ? 0 : right_speed;

  // Run the motors
  left_motor->run(BACKWARD);
  right_motor->run(BACKWARD);
  left_motor->setSpeed(left_speed);
  right_motor->setSpeed(right_speed);

  // Only print data every second
  counter++;
  if (counter % 100 == 0)
    {
      counter = 0;
      Serial.print(left);
      Serial.print(",");
      Serial.print(right);
      Serial.print(",");
      Serial.print(left_speed);
      Serial.print(",");
      Serial.println(right_speed);
    }
  delay(10);
}
